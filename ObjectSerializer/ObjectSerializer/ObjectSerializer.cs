﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace ObjectSerializer
{
    public static class ObjectSerializer<T>
    {
        public static void SerializeBinaryObject(T x, string filename)
        {
            using (Stream stream = File.Open(filename, FileMode.Create, FileAccess.Write))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, x);
            }
        }

        public static T DeserializeBinaryObject(string filename)
        {
            using (Stream stream = File.Open(filename, FileMode.Open))
            {
                var binaryFormatter = new BinaryFormatter();
                var obj = (T)binaryFormatter.Deserialize(stream);
                return obj;
            }
        }

        public static void SerializeXmlObject(T t, string filename)
        {
            using (Stream stream = File.Open(filename, FileMode.Create, FileAccess.Write))
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                xmlSerializer.Serialize(stream, t);
            }
        }

        public static T DeserializeXmlObject(string filename)
        {
            using (Stream stream = File.Open(filename, FileMode.Open))
            {
                var xmlDeserializer = new XmlSerializer(typeof(T));
                var obj = (T)xmlDeserializer.Deserialize(stream);
                return obj;
            }
        }

    }
}
