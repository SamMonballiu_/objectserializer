﻿using System;

namespace TestApp
{
    [Serializable]
    public class Person
    {
        public Person() { }

        public Person(string name, string hobby, int age)
        {
            Name = name;
            Hobby = hobby;
            Age = age;
        }

        public string Name { get; set; }
        public string Hobby { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"{Name} ({Age}) likes {Hobby}.";
        }
    }
}
