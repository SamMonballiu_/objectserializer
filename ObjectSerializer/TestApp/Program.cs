﻿using ObjectSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> tests = new List<string>() { "Copy and connect the repository locally", "Learn more or clone in SourceTree" };
            string teststring = "Hi I am being tested!";
            Person testperson = new Person("Kenan", "Teaching", 25);
            try
            {

                ObjectSerializer<List<string>>.SerializeBinaryObject(tests, "tests.bin");
                ObjectSerializer<List<string>>.SerializeXmlObject(tests, "tests.xml");
                ObjectSerializer<string>.SerializeBinaryObject(teststring, "teststring.bin");
                ObjectSerializer<Person>.SerializeXmlObject(testperson, "testPerson.xml");

            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }

            try
            {
                List<string> loadedList = ObjectSerializer<List<string>>.DeserializeBinaryObject("tests.bin");
                string loadedString = ObjectSerializer<string>.DeserializeBinaryObject("teststring.bin");
                Person loadedPerson = ObjectSerializer<Person>.DeserializeXmlObject("testPerson.Xml");

                Console.WriteLine("\nLoaded values:");
                loadedList.ForEach(x => Console.WriteLine(x));
                Console.WriteLine(loadedString);
                Console.WriteLine(loadedPerson);
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }

                Console.ReadKey();
        }
    }
}
